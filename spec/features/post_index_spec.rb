# frozen_string_literal: true

require 'rails_helper'
describe CommentsController do
  before do
    @user = FactoryBot.create(:user, admin: true)
  end
  it "creates a post and verifies it is displaying in index page or not" do
    visit "/users/sign_in"
    within(".new_user") do
      fill_in "Email", :with => @user.email
      fill_in "Password", :with => "testtest"
    end
    click_button "Log in"
    visit "/posts/new"
    fill_in "Title", :with => "abcdef"
    fill_in "Summary", :with => "this is test post"
    click_button "Create Post"
    visit "/posts"
    expect(page.body).to have_text("abcdef")
    expect(page.body).to have_text("this is test post")
  end
end
