# frozen_string_literal: true

require 'rails_helper'
describe CommentsController do
  before(:each) do
    @post = FactoryBot.create(:post)
    @comment = FactoryBot.create(:comment, post_id: @post.id)
  end

  context "loggedout user" do

    it "donot allow create comment" do 
      post :create, :params =>  {comment: { name: "asd", body: "asdw" }, post_id: @post.id }
      expect(response).to redirect_to('/users/sign_in')
      expect(Comment.count).to be(1)
    end

    it "donot allow update Comments" do
      patch :update, :params => { comment: { name: "asd", body: "asdqw" }, post_id: @post.id, id: @comment.id }
      expect(response).to redirect_to('/users/sign_in')
      expect(Comment.count).to be(1)
    end

    it "donot allow delete Comments" do
      delete :destroy, :params => { post_id: @post.id, id: @comment.id }
      expect(response).to redirect_to('/')
      expect(Comment.count).to be(1)
    end
  end
  
  context "loggedin user" do
    before do
      @user = FactoryBot.create(:user)
      sign_in @user
    end

    it "allow create Comments" do 
      post :create, :params => { comment: { name: "asd", body: "asdw" }, post_id: @post.id }
      expect(Comment.last.name).to eq("asd")
      expect(Comment.count).to be(2)
    end

    it "allow update Comments" do
      post :update, :params => { comment: { name: "test", body: "asdqw" }, post_id: @post.id, id: @comment.id }
      expect(Comment.count).to be(1)
      expect(Comment.last.name).to eq("test")
    end

    it "allow delete Comments" do
      delete :destroy, :params => { post_id: @post.id, id: @comment.id }
      expect(response).to redirect_to('/')
      expect(Comment.count).to be(1)
    end
  end
  
  context "admin user" do
    before do
      @user = FactoryBot.create(:user, :admin => true)
      sign_in @user
    end

    it "allow create Comments" do 
      post :create, :params => { comment: { name: "asd", body: "asdw" }, post_id: @post.id }
      expect(response).to redirect_to('/posts/'+@post.title)
      expect(Comment.count).to be(2)
      expect(Comment.last.name).to eq("asd")
    end

    it "allow update Comments" do
      post :update, :params => { comment: { name: "test", body: "asdqw" }, post_id: @post.id, id: @comment.id }
      expect(Comment.count).to be(1)
      expect(Comment.last.name).to eq("test")
    end
    
    it "allow delete Comments" do
      delete :destroy, :params => { post_id: @post.id, id: @comment.id }
      expect(response).to redirect_to('/posts/'+@post.title)
      expect(Comment.count).to be(0)
    end
  end
end
