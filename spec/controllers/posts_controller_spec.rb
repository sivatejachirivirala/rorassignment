# frozen_string_literal: true

require 'rails_helper'
describe PostsController do
  before(:each) do
    @post = FactoryBot.create(:post)
  end

  context "loggedout user" do
    it "allow shows a post" do
      get :show, :params => { id:  @post.id  }
      expect(response).to be_successful
      expect(assigns(:post)).to eq(@post)
    end

    it "allow create posts" do 
      post :create, :params => { post: { title: "abedc", summary: "teja" } }
      expect(response).to redirect_to('/users/sign_in')
      expect(Post.count).to be(2)
    end

    it "allow update posts" do
      patch :update, :params => { id:  @post.id ,post: { title: "teste", summary: "teja" } }
      expect(response).to redirect_to('/users/sign_in')
      expect(Post.count).to be(2)
    end

    it "allow delete posts" do
      delete :destroy, :params => { id:  @post.id }
      expect(response).to redirect_to('/')
      expect(Post.count).to be(2)
    end
  end
  
  context "loggedin user" do
    before do
      @user = FactoryBot.create(:user)
      sign_in @user
    end

    it "allow shows a post" do
      get :show, :params => { id:  @post.id  }
      expect(response).to be_successful
      expect(assigns(:post)).to eq(@post)
    end

    it "allow create posts" do 
      post :create, :params => { post: { title: "abedc", summary: "teja" } }
      expect(response).to redirect_to('/posts/abedc')
      expect(Post.last.title).to eq("abedc")
      expect(Post.count).to be(3)
    end

    it "allow update posts" do
      post :update, :params => { id:  @post.id ,post: { title: "teste", summary: "teja" } }
      @post.reload
      expect(Post.count).to be(2)
      expect(response).to redirect_to('/posts/teste')
      expect(@post.title).to eq("teste")
    end

    it "allow delete posts" do
      delete :destroy, :params => { id:  @post.id }
      expect(response).to redirect_to('/')
      expect(Post.count).to be(2)
    end
  end
  
  context "admin user" do
    before do
      @user = FactoryBot.create(:user, :admin => true)
      sign_in @user
    end

    it "allow shows a post" do
      get :show, :params => { id:  @post.id  }
      expect(response).to be_successful
      expect(assigns(:post)).to eq(@post)
    end

    it "allow create posts" do 
      post :create, :params => { post: { title: "abedc", summary: "teja" } }
      expect(response).to redirect_to('/posts/abedc')
      expect(Post.count).to be(3)
      expect(Post.last.title).to eq("abedc")
    end

    it "allow update posts" do
      post :update, :params => { id:  @post.id ,post: { title: "teste", summary: "teja" } }
      @post.reload
      expect(Post.count).to be(2)
      expect(response).to redirect_to('/posts/teste')
      expect(@post.title).to eq("teste")
    end

    it "allow delete posts" do
      delete :destroy, :params => { id:  @post.id }
      expect(response).to redirect_to('/posts')
      expect(Post.count).to be(1)
    end
  end
end
