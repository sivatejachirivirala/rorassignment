# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    title {"testpost"}
    summary {"This is a test blogpost"}
  end
end
