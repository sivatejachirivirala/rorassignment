# frozen_string_literal: true

FactoryBot.define do
  factory :comment do
    name {"test"}
    body {"This is a test comment"}
  end
end
