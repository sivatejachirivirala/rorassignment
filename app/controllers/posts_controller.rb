# frozen_string_literal: true

class PostsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!, except: [:index, :show]

  def new
    @post = Post.new
  end

  def show
    @post = Post.friendly.find(params[:id])
  end

  def index
    @posts = Post.paginate(page: params[:page], per_page: 5)
  end

  def create
    @post = Post.new(post_params)
    if @post.save!
      redirect_to @post, notice: t('post_created')
    else
      render 'new'
    end
  end

  def update
    @post = Post.friendly.find(params[:id])
    if @post.update!(post_params)
      redirect_to @post, notice: t('post_updated')
    else
      render 'edit'
    end
  end

  def edit
    @post = Post.friendly.find(params[:id])
  end

  def destroy
    @post = Post.friendly.find(params[:id])
    @post.destroy!
    redirect_to posts_path, notice: t('post_deleted')
  end

  
  private

    def post_params
      params.require(:post).permit(:title, :summary)
    end
end
