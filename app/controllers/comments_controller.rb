# frozen_string_literal: true

class CommentsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!, except: [:index, :show]

  def new
    @post = Post.friendly.find(params[:post_id])
  end

  def create
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.create!(comment_params)
    redirect_to post_path(@post), notice: t('comment_created')
  end

  def destroy
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy!
    redirect_to post_path(@post), notice: t('comment_deleted')
  end

  def edit
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
  end

  def update
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    if @comment.update!(comment_params)
      redirect_to post_path(@post), notice: t('comment_updated')
    else
      render 'edit'
    end
  end

  private

    def comment_params
      params.require(:comment).permit(:name, :body)
    end
end
