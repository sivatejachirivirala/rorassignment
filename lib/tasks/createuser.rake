def prompt(message)
    print(message)
    STDIN.gets.chop
end
    
namespace :createuser do
    task :createuser => :environment do
        email = prompt('Email: ')
        password = prompt('Password: ')
    
        user = User.new(email: email, password: password)
        unless user.save
            STDERR.puts('Cannot create a new user:')
            user.errors.full_messages.each do |message|
                STDERR.puts(" * #{message}")
            end
        end
    end
end